/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium5;

/**
 *
 * @author pracownik
 */
public class Stos {
    private int [] tablica;
    private int wskaznik;
    private int maksymalnyRozmiar;
    
    Stos(int rozmiar) {
        tablica = new int[1];
        maksymalnyRozmiar = rozmiar;
        wskaznik = 0;
    }
    
    public int pop() throws Exception {
        if (wskaznik > 0) {
            int elem = tablica[tablica.length - 1];
            int [] nowaTablica = new int[tablica.length - 1];
            for (int i = 0; i < tablica.length - 1; i++) {
                nowaTablica[i] = tablica[i];
            }
            tablica = nowaTablica;
            wskaznik--;
            return elem;
        }
        throw new Exception("Stos jest pusty i nie można z niego zdejmować");
    }
    
    public void push(int elem) {
       if (wskaznik < maksymalnyRozmiar) {
            int [] nowaTablica = new int[wskaznik + 1];
            for (int i = 0; i < wskaznik; i++) {
                 nowaTablica[i] = tablica[i];
             }
            nowaTablica[wskaznik] = elem;
            wskaznik++;
            tablica = nowaTablica;
       } else {
           System.out.println("Przekroczono rozmiar stosu");
       }
       
    }
}
