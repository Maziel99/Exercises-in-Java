/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium5;

/**
 *
 * @author pracownik
 */
public class Ksiazka {
    private final String tytul, autor, rokWydania;
    private double cena;
    private final int liczbaStron;
    
    Ksiazka(String tytul,String autor, String rokWydania,
            double cena,int liczbaStron) {
        this.tytul = tytul;
        this.autor = autor;
        this.rokWydania = rokWydania;
        this.liczbaStron = liczbaStron;
        this.cena = cena;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }
    
    public String getTytul() {
        return tytul;
    }

    public String getAutor() {
        return autor;
    }

    public String getRokWydania() {
        return rokWydania;
    }

    public int getLiczbaStron() {
        return liczbaStron;
    }
    
    // wyświetli się błąd!!!
    /*public void setTytul(String tytul) {
        this.tytul = tytul;
    }*/

    
    
}
