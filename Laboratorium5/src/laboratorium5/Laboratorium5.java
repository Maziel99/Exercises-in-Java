/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium5;

/**
 *
 * @author pracownik
 */
public class Laboratorium5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Ksiazka ksiazka = new Ksiazka("Pan Tadeusz", "Adam Mickiewicz", "2020",
                                      70.5, 150);
        // możemy zmieniać jedynie pole cena
        // pozostałe pola są tylko do odczytu - modyfikator final
        ksiazka.setCena(80);
        System.out.println(ksiazka.getCena());
        
        System.out.println(ksiazka.getAutor());
        System.out.println(ksiazka.getLiczbaStron());
        System.out.println(ksiazka.getRokWydania());
        System.out.println(ksiazka.getTytul());
        
        Stos stos = new Stos(3);
        stos.push(1);
        stos.push(2);
        stos.push(3);
        
        try {
            System.out.println("Zdjęto " + stos.pop() + " ze stronu");
            System.out.println("Zdjęto " + stos.pop() + " ze stronu");
            System.out.println("Zdjęto " + stos.pop() + " ze stronu");

            // próba zdjęcia ze stosu pustego
            System.out.println("Zdjęto " + stos.pop() + " ze stronu");
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
        stos.push(4);
        stos.push(5);
        stos.push(6);
        // próba przekroczenia rozmiaru stosu
        stos.push(4);
        
        Liczba liczba = new Liczba(123);
        liczba.pokazCyfry();
        System.out.println("");
        System.out.print("123 * 20 = ");
        liczba.mnozenie(new Liczba(20)).pokazCyfry();
        System.out.println("");
        System.out.print("528 * 436 = ");
        new Liczba(528).mnozenie(new Liczba(436)).pokazCyfry();
        System.out.println("");
        
        Data data = new Data();
        data.get();
        data.zmień();
        data.get();
    }
    
}
