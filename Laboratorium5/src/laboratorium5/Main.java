/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium5;

import java.util.Scanner;

/**
 *
 * @author Ja
 */
public class Main {
    public static void main(String [] args){
        Firma firma = new Firma();
        Scanner scanner = new Scanner(System.in);
        int x = 0;

        while(true){
            System.out.println("Wybierz opcję\n");
            System.out.println("1 - Dodaj pracownika");
            System.out.println("2 - Wyświetl pracowników");

            x = scanner.nextInt();

            switch(x){
                case 1: {
                    firma.dodaj();
                    break;
                }
                case 2: {
                    firma.pokaz();
                    break;
                }
                default: {
                    System.out.println("Nieprawidłowa operacja");
                    break;
                }
            }
        }
    }
}
