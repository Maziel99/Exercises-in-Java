/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium5;

import java.util.Calendar;
import java.util.Scanner;

/**
 *
 * @author Ja
 */
public class Data {
    private int d, m, y;
    private Calendar kalendarz = Calendar.getInstance();

    public Data(){
        this.d = kalendarz.get(Calendar.DAY_OF_MONTH);
        this.m = kalendarz.get(Calendar.MONTH) + 1;
        this.y = kalendarz.get(Calendar.YEAR);
    }

    public void get(){
        System.out.println(this.d + "." + this.m + "." + this.y);
    }

    public void zmień(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Zmień o tydzień");
        System.out.println("1 - w przód");
        System.out.println("2 - w tył");
        int x = scanner.nextInt();

        switch (x) {
            case 1: {
                kalendarz.add(Calendar.DATE, 7);
                break;
            }
            case 2: {
                kalendarz.add(Calendar.DATE, -7);
                break;
            }
            default:
                System.out.println("Podaj poprawne dane");
        }
        this.d = kalendarz.get(Calendar.DAY_OF_MONTH);
        this.m = kalendarz.get(Calendar.MONTH) + 1;
        this.y = kalendarz.get(Calendar.YEAR);
    }
}
