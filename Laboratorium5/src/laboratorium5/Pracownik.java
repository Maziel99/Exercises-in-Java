/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium5;

import java.util.Scanner;

/**
 *
 * @author Ja
 */
public class Pracownik {
    public String imie, nazwisko, stanowisko;
    public int wiek;
    Scanner scanner = new Scanner(System.in);

    public Pracownik(String imie, String nazwisko,String stanowisko, int wiek){
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.stanowisko = stanowisko;
        this.wiek = wiek;
    }

    public Pracownik(){

    }

    public void wyswietl(){
        System.out.println("Imię: " + imie);
        System.out.println("Nazwisko: " + nazwisko);
        System.out.println("Stanowisko: " + stanowisko);
        System.out.println("Wiek: " + wiek);
    }

    protected void dodaj(Pracownik [] pracownik, int id){
        pracownik[id] = new Pracownik();
        System.out.println("Imię: ");
        pracownik[id].imie = scanner.next();
        System.out.println("Nazwisko: ");
        pracownik[id].nazwisko = scanner.next();
        System.out.println("Stanowisko:");
        pracownik[id].stanowisko = scanner.next();
        System.out.println("Wiek: a");
        pracownik[id].wiek = scanner.nextInt();
        id++;
        System.out.println("Zapisano:");
        if(id == 100) System.out.println("Rejestr jest pełny");
    }

    protected void pokaz(Pracownik [] lista, int id){
        for(int i = 0; i < id; i++){
            System.out.println("Dane pracownika nr " + (i+1));
            System.out.println(lista[i]);
        }
    }

    public String toString(){
        return "Imię: " + this.imie + ", nazwisko: " + this.nazwisko + ", stanowisko: " + this.stanowisko+ ", wiek: " + this.wiek;
    }
}

