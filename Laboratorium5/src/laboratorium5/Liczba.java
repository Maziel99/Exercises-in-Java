/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium5;

/**
 *
 * @author pracownik
 */
public class Liczba {
    private int [] cyfry;
    
    public Liczba(int liczba) {
        // zakładamy, że liczba musi być reprezentowana przez co
        // najmniej jedną cyfrę
        int liczbaCyfr = 1;
        int liczbaPomocnicza = liczba;
        
        // sprawdzamy do jakiej potęgi trzeba będzie podnieść 10,
        // aby reprezentować naszą liczbę
        // dopóki wynik dzielenia liczby przez 10 nie jest ułamkiem
        // oznacza to, że potrzeba jeszcze cyfr
        while (liczbaPomocnicza / 10 > 0) {
            liczbaCyfr++;
            liczbaPomocnicza /= 10;
        } 
        
        cyfry = new int[liczbaCyfr];
        
        // przykład dla liczby trzycyfrowej
        // pierwsza cyfra to cyfra setek, dlatego dzielimy liczbę przez 100
        /*cyfry[0] = (int) (liczba / Math.pow(10, 2));
        // zmiejszamy liczbę o część setną 
        liczba -= cyfry[0] * Math.pow(10, 2);
        // druga cyfra to liczba dziesiątek, dlatego dzielimy liczbę przez 10
        cyfry[1] = (int) (liczba / Math.pow(10, 1));
        // zmiejszamy liczbę o część dziesiętną 
        liczba -= cyfry[1] * Math.pow(10, 1);
        cyfry[2] = liczba;*/
        
        // uogólniony wzór z komentarza
        for (int i = cyfry.length - 1; i > -1; i--) {
            cyfry[(cyfry.length - 1) - i] = (int) (liczba / Math.pow(10, i));
            liczba -= cyfry[(cyfry.length - 1) - i] * Math.pow(10, i);
        }
    }
    
    public Liczba(String liczba) {
        
    }
    
    public int liczbaCyfr() {
        return this.cyfry.length;
    }
    
    public void pokazCyfry() {
        for (int c: cyfry) {
            System.out.print(c);
        }
    }
    
    public Liczba mnozenie(Liczba b) {
        // przykłady
        // 121 * 20 = (100 + 20 + 1) * 20 = (100 * 20) + 20 * 20 + 1 * 20 =
        // (1 * 10^2 * 2 * 10^1) + (2 * 10^1 * 2 * 10^1) + (1 * 10^0 + 2 * 10^1)
        
        // 123 * 567 = (100 + 20 + 3) * (500 + 60 + 7) = (100 * 500 + 100 * 60 +
        // 100 * 7) + (20 * 500 + 20 * 60 + 20 * 7) + (3 * 500 + 3 * 60 + 3 * 7) =
        // (1 * 10^2 * 5 * 10^2 + 1 * 10^2 * 6 * 10^1 + 1 * 10^2 * 7 * 10^0) +
        // (2 * 10^1 * 5 * 10^2 + 2 * 10^1 * 6 * 10^1 + 2 * 10^1 * 7 * 10^0) +
        // (3 * 10^0 * 5 * 10^2 + 3 * 10^0 * 6 * 10^1 + 3 * 10^0 * 7 * 10^0)
        
        int mnozenie = 0;
        for (int i = 0; i < this.cyfry.length; i++) {
            for (int j = 0; j < b.cyfry.length; j++)
                mnozenie += b.cyfry[j] * Math.pow(10, b.cyfry.length - 1 - j) * 
                            this.cyfry[i] * Math.pow(10, this.cyfry.length - 1 - i);
            //System.out.println(mnozenie);
        }
        
    Liczba liczba = new Liczba(mnozenie);
    return liczba;
    }
}
