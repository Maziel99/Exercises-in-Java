/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium2;

import java.util.Scanner;

/**
 *
 * @author Ja
 */
public class zadanie4 {
    public static void main(String[] args) {
        // TODO code application logic here
        double [] tablica = new double[10];
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program wczytuje 10-elementową tablicę typu double");
        for (int i = 0; i < tablica.length; i++) {
            System.out.print("element " + (i + 1) + " = ");
            tablica[i] = scanner.nextDouble();
        
    }
        
        while(true){
            System.out.println("Co chcesz zrobić?");
            System.out.println("1 - Suma elementów tablicy");
            System.out.println("2 - Iloczyn elementów tablicy");
            System.out.println("3 - Średnia elementów tablicy");
            System.out.println("4 - Element minimalny tablicy");
            System.out.println("5 - Element maksymalny tablicy");
            
            
            int menu = scanner.nextInt();
            
            if(menu == 1){
                double suma = 0;
                for (int i = 0; i<tablica.length; i++)
                {
                    suma += tablica[i];
                }
                System.out.println("Suma liczb wynosi: " + suma);
            }
                
            if(menu == 2){
                double iloczyn = 1;
                for (int i = 0; i<tablica.length; i++)
                {
                    iloczyn *= tablica[i];
                }
                System.out.println("Iloczyn liczb wynosi: " + iloczyn);
            }
                
            if(menu == 3){
                double suma = 0;
                for (int i = 0; i<tablica.length; i++)
                {
                    suma += tablica[i];
                }
                double srednia = suma / tablica.length;
                System.out.println("Średnia liczb wynosi: " + srednia);
            }
                
            if(menu == 4){
                double min = Double.MAX_VALUE;
                for (int i = 0; i < tablica.length; i++) {
                    if(tablica[i] < min) {
                        min = tablica[i];
                    }
                }
                System.out.println("Minimum wynosi:" + min);
            }
                
            if(menu == 5){
                double max = Double.MIN_VALUE;
                for (int i = 0; i < tablica.length; i++) {
                    if(tablica[i] > max) {
                        max = tablica[i];
                    }
                }
                System.out.println("Maximum wynosi:" + max);
            }
            else
                System.out.println("Zły parametr, podaj odpowiednią cyfrę");
                
        }
    }
}