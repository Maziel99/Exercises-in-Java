/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium2;

import java.util.Scanner;

/**
 *
 * @author Ja
 */
public class zadanie3 {
    public static void main(String[] args){
        double [] tablica = new double[10];
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program wczytuje 10-elementową tablicę typu double");
        for (int i = 0; i < tablica.length; i++) {
            System.out.print("element " + (i + 1) + " = ");
            tablica[i] = scanner.nextDouble();
        }
        
        while(true){
            System.out.println("Co chcesz zrobić?");
            System.out.println("1 - wypisz tablicę od początku");
            System.out.println("2 - wypisz tablicę od końca");
            System.out.println("3 - wypisz elementy o indeksach nieparzystych");
            System.out.println("4 - wypisz elementy o indeksach parzystych");
            
            int menu = scanner.nextInt();
            
            
            if(menu == 1){
                for (double d : tablica) {
                    System.out.print(d + ", ");
                 }
                   System.out.println("");
            }
            
     // bierzemy length - 1, bo indeksujemy od zera, np. tablica 10 elementowa
        // ma największy indeks 9, a nie 10
         // i > -1 - warunek kontyuowania pętli
         // i-- - dekrementacja, zmiejszanie wartości o 1, a nie inkrementacja
         // czyli zwiększanie wartości o 1
            if(menu == 2){
                for (int i = tablica.length - 1; i > -1; i--) {
                    System.out.print(tablica[i] + ", ");
                }
            
                System.out.println("");
            }
            
        if(menu == 3){
            for (int i = 0; i < tablica.length; i++) {
                     if (i % 2 == 0) {
                    System.out.println("element o indeksie " + i + " = " + tablica[i]);
                
                }                                              
            }
            System.out.println("");
        }
        
                
        if(menu == 4){
            for (int i = 0; i < tablica.length; i++) {
                     if (i % 2 != 0) {
                        System.out.println("element o indeksie " + i + " = " + tablica[i]);
                
                    }                                                 
            }
             System.out.println("");
        }
        else
                System.out.println("Zły parametr, podaj odpowiednią cyfrę");
        

    }
}

        }
        
         // równoważne
         /*int i = 0;
           do {
             System.out.print("element " + (i + 1) + " = ");
             tablica[i] = scanner.nextDouble();
              i++;
          } while(i < tablica.length);
  
        // równoważne
        i = 0;
        while (i < tablica.length) {
            System.out.print("element " + (i + 1) + " = ");
            tablica[i] = scanner.nextDouble();
            i++;
        }*/

         
        