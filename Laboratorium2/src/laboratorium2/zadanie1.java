/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium2;

import java.util.Scanner;

/**
 *
 * @author Ja
 */
public class zadanie1 {
    public static void main(String[] args) {
        double a, b, c, delta;
        
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program rozwiązuję trójmian kwadratowy");
        System.out.print("Podaj współczynnik a = ");
        a = scanner.nextDouble();
        if (a == 0) {
            System.out.println("a nie może być równe zero");
            return ;
        }
        System.out.print("Podaj współczynnik b = ");
        b = scanner.nextDouble();
        System.out.print("Podaj współczynnik c = ");
        c = scanner.nextDouble();
        delta = b * b - 4 * a * c;
        if (delta < 0) {
            System.out.println("Delta ujemna, brak rozwiązania");
            return ;
        }
        double pierwiastek_delta = Math.sqrt(delta);
        double x1 = (-b - pierwiastek_delta) / (2 * a);
        double x2 = (-b + pierwiastek_delta) / (2 * a);
        System.out.println("Rozwiązanie równania x1 = " + x1 + " , x2 = " + x2);
        
    }
}
