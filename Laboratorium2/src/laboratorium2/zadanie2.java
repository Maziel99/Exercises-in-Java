/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium2;

import java.util.Date;
import java.util.Scanner;

/**
 *
 * @author Ja
 */
public class zadanie2 {
    
    public static double dodaj(double a, double b) {
        return a + b;
    }
    
    public static double odejmij(double a, double b) {
        return a - b;
    }
    
    public static double pomnóż(double a, double b) {
        return a * b;
    }
    
    public static double podziel(double a, double b) {
        return a / b;
    }
    
    public static double potęguj(double a, double b) {
        return Math.pow(a, b);
    }
    
    public static double pierwiastek(double a) {
        return Math.sqrt(a);
    }
     
    public static void main(String[] args) {
        String dzialanie = "";
        Scanner scanner = new Scanner(System.in);
        while (!dzialanie.equals("end")) {
            System.out.println("Co chcesz zrobić?");
            System.out.println("+ - suma");
            System.out.println("- - różnica");
            System.out.println("* - iloczyn");
            System.out.println("/ - iloraz");
            System.out.println("pot - potęga");
            System.out.println("prw - pierwiastek kwadratowy");
            System.out.println("sin - sinus (miara łukowa");
            System.out.println("cos - cosinus (miara łukowa");
            System.out.println("tg - tangens (miara łukowa");
            System.out.println("ctg - cotangens (miara łukowa");
            
            dzialanie = scanner.next();
            double a, b;
            switch(dzialanie) {
                case "+": {
                    a = scanner.nextDouble();
                    b = scanner.nextDouble();
                    System.out.println(dodaj(a, b));
                    break;
                } 
                case "-": {
                    a = scanner.nextDouble();
                    b = scanner.nextDouble();
                    System.out.println(odejmij(a, b));
                    break;
                } 
                case "*": {
                    a = scanner.nextDouble();
                    b = scanner.nextDouble();
                    System.out.println(pomnóż(a, b));
                    break;
                } 
                case "/": {
                    a = scanner.nextDouble();
                    b = scanner.nextDouble();
                    System.out.println(podziel(a, b));
                    break;
                } 
                case "pot": {
                    a = scanner.nextDouble();
                    b = scanner.nextDouble();
                    System.out.println(potęguj(a, b));
                    break;
                } 
                case "prw": {
                    a = scanner.nextDouble();
                    System.out.println(pierwiastek(a));
                    break;
                }
                case "sin": {
                    a = scanner.nextDouble();
                    System.out.println(Math.sin(a));
                    break;
                }
                case "cos": {
                    a = scanner.nextDouble();
                    System.out.println(Math.cos(a));
                    break;
                }
                case "tg": {
                    a = scanner.nextDouble();
                    System.out.println(Math.tan(a));
                    break;
                }
                case "ctg": {
                    a = scanner.nextDouble();
                    System.out.println(1/Math.tan(a));
                    break;
                }
                case "end": { System.out.println("Koniec działania programu!");} break;
                default: System.out.println("Niepoprawne działanie!");break;
            }
        }
    }
}
