/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium2;

import java.util.Scanner;

/**
 *
 * @author Ja
 */
public class zadanie7 {
    public static void main(String[] args){
        double [] tablica = new double[4];
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program wczytuje 4-elementową tablicę typu double");
        for (int i = 0; i < tablica.length; i++) {
            System.out.print("element " + (i + 1) + " = ");
            tablica[i] = scanner.nextDouble();
        }
        
        for (int i = 0; i < tablica.length; i++) {
            for (int j = 0; j < tablica.length; j++) {
                if (tablica[j] > tablica[i]) {
                    double temp = tablica[i];
                    tablica[i] = tablica[j];
                    tablica[j] = temp;
                }
            }
        }
        
        for (double d : tablica) {
            System.out.print(d + ", ");
        }
        
        //sortowanie przez wstawianie
       for (int i = 1; i < tablica.length; i++) {
           int j = i - 1;
           double wstawianyElement = tablica[i];
           while (j > -1 && tablica[j] > wstawianyElement) {
               tablica[j + 1] = tablica[j];
               j--;
           }
           tablica[j + 1] = wstawianyElement;
       }
           for (double d : tablica)  {
               System.out.print(d + ", ");
           }
       }
                   
        
    }

