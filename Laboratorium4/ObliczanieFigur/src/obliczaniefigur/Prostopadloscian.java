/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package obliczaniefigur;

/**
 *
 * @author Ja
 */
public class Prostopadloscian {
    public double a, b, H;
    
    public Prostopadloscian(double a, double b, double H){
        this.a = a;
        this.b = b;
        this.H = H;
    }
    
    public double pole(){
        return 2*a*b+2*b*H+2*a*H;
    }
    
    public double objetosc(){
        return a*b*H;
    }
}
