/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package obliczaniefigur;

/**
 *
 * @author Ja
 */
public class Kula {
    public double R;
    
    public Kula(double R){
        this.R = R;
    }
    
    public double pole(){
        return 4*Math.PI*(R*R);
    }
    
    public double objetosc(){
        return (4/3)*Math.PI*(R*R*R);
    }
}
