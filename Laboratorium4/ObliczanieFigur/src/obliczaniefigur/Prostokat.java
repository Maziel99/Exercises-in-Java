/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package obliczaniefigur;

/**
 *
 * @author Ja
 */
public class Prostokat {
    public double a,b;
    
    public Prostokat(double a, double b){
        this.a = a;
        this.b = b;
    }
    
    public double pole(){
        return a*b;
    }
    
    public double obwod(){
        return 2*a+2*b;
    }
}
