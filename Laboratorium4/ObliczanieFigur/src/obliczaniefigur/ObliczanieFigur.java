/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package obliczaniefigur;

/**
 *
 * @author Ja
 */
public class ObliczanieFigur {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Kwadrat kwadrat = new Kwadrat(6);
        Prostokat prostokat = new Prostokat(7,3);
        Kolo kolo = new Kolo(5);
        Szescian szescian = new Szescian(8);
        Prostopadloscian prostopadloscian = new Prostopadloscian(9,3,9);
        Kula kula = new Kula(4);
        Stozek stozek = new Stozek(5, 6, 4);
        
        
        System.out.println("Pole kwadratu: "+ kwadrat.pole());
        System.out.println("Obwod kwadratu: "+ kwadrat.obwod());
        System.out.println("Bok kwadratu: "+ kwadrat.a);
        
        System.out.println("Pole prostokata: "+ prostokat.pole());
        System.out.println("Obwod prostokata: "+ prostokat.obwod());
        System.out.println("Boki prostokata: "+ prostokat.a + " " +prostokat.b);
        
        System.out.println("Pole kola: "+ kolo.pole());
        System.out.println("Obwod kola: "+ kolo.obwod());
        System.out.println("Promien kola: "+ kolo.r);
        
        System.out.println("Pole szescianu: "+ szescian.pole());
        System.out.println("Objetosc szescianu: "+ szescian.objetosc());
        System.out.println("Bok szescianu: "+ szescian.a);
        
        System.out.println("Pole prostopadloscianu: "+ prostopadloscian.pole());
        System.out.println("Objetosc prostopadloscianu: "+ prostopadloscian.objetosc());
        System.out.println("Boki prostopadloscianu: "+ prostopadloscian.a + " " + prostopadloscian.b + " " + prostopadloscian.H);
        
        System.out.println("Pole kuli: "+ kula.pole());
        System.out.println("Objetosc kula: "+ kula.objetosc());
        System.out.println("Promien kuli: "+ kula.R);
        
        System.out.println("Pole stozka: "+ stozek.pole());
        System.out.println("Objetosc stozka: "+ stozek.objetosc());
        System.out.println("Wymiary stożka: "+ stozek.r + " " + stozek.l + " " + stozek.H);
        
    }
    
}
