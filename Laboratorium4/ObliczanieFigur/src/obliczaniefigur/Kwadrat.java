/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package obliczaniefigur;

/**
 *
 * @author Ja
 */
public class Kwadrat {
    
    public double a;
    
    public Kwadrat(double a){
        this.a = a;
    }
    
    public double pole(){
        return a*a;
    }
    
    public double obwod(){
        return a*4;
    }
}
