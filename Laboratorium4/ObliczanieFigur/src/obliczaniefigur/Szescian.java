/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package obliczaniefigur;

/**
 *
 * @author Ja
 */
public class Szescian {
    public double a;
    
    public Szescian(double a){
        this.a = a;
    }
    
    public double pole(){
        return a*a*6;
    }
    
    public double objetosc(){
        return a*a*a;
    }
}
