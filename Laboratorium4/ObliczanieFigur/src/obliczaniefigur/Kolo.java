/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package obliczaniefigur;

/**
 *
 * @author Ja
 */
public class Kolo {
    public double r;
    
    public Kolo(double r){
        this.r = r;
    }
    
    public double pole(){
        return r*r*Math.PI;
    }
    
    public double obwod(){
        return 2*Math.PI*r;
    }
}
