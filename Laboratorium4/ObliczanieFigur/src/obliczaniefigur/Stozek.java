/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package obliczaniefigur;

/**
 *
 * @author Ja
 */
public class Stozek {
    public double r, l, H;
    
    public Stozek(double r, double l, double H){
        this.r = r;
        this.l = l;
        this.H = H;
    }
    
    public double pole(){
        return (Math.PI*r*r)+(Math.PI*r*l);
    }
    
    public double objetosc(){
        return (Math.PI*r*r*H)/3;
    }
}
