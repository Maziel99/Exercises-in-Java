/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium4;

/**
 *
 * @author pracownik
 */
public class StudentDziedziczenie extends Osoba {
    String nr_indeksu;
    String rok_studiow;
    String specjalizacja;
    
    StudentDziedziczenie(String imie, String nazwisko, int wiek, String nr_indeksu, 
                         String rok_studiow, String specjalizacja) {
        super(imie, nazwisko, wiek);
        this.nr_indeksu = nr_indeksu;
        this.rok_studiow = rok_studiow;
        this.specjalizacja = specjalizacja;
    }
    
    public void pokazDane() {
        super.pokazDane();
        System.out.println("nr indeksu " + this.nr_indeksu);
        System.out.println("rok studiów " + this.rok_studiow);
        System.out.println("specjalizacja " + this.specjalizacja);
    }
}
