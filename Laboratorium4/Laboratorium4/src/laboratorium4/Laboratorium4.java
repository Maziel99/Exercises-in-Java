/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium4;

/**
 *
 * @author pracownik
 */
public class Laboratorium4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Osoba osoba1 = new Osoba("Jan", "Kowalski", 15);
        osoba1.pokazDane();
        
        Osoba osoba2 = new Osoba("Jan", "Nowak");
        osoba2.pokazDane();
        
        osoba2.imie = "Stefan";
        osoba2.nazwisko = "Kowalski";
        osoba2.wiek = 70;
        
        osoba2.pokazDane();
        
        StudentDziedziczenie studentDz = new StudentDziedziczenie("Jan", 
                "Kowalski", 23, "000000", "III", "Aplikacje internetowe");
        studentDz.pokazDane();
    }
    
}
