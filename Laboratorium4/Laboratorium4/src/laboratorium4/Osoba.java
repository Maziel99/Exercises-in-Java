/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium4;

/**
 *
 * @author pracownik
 */
public class Osoba {
    public String imie, nazwisko;
    public int wiek;
    
    public Osoba(String imie_, String nazwisko_, int wiek_) {
        imie = imie_;
        nazwisko = nazwisko_;
        wiek = wiek_;
    }
    
    public Osoba(String imie_, String nazwisko_) {
        imie = imie_;
        nazwisko = nazwisko_;
    }
    public Osoba() {
    
    }
    
    public void pokazDane() {
        System.out.println("Osoba");
        System.out.println("imię " + this.imie);
        System.out.println("nazwisko " + this.nazwisko);
        System.out.println("wiek " + this.wiek);
    }
}
