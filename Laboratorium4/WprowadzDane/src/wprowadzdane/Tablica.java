/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wprowadzdane;

import java.util.Scanner;

/**
 *
 * @author Ja
 */
public class Tablica {
    public Student [] tablica = new Student[100];
    private Scanner scanner = new Scanner(System.in);
    private int numer;
    private int poczatek;
    private int koniec;
    private String wybor;

    protected void tablica(){
        for(int i = 0; i < tablica.length; i++){
            tablica[i] = new Student("", "", 0, "", 0);
        }
    }

    public void tworzenie() {
        System.out.println("Podaj indeks (0-99)");
        numer = scanner.nextInt();

        System.out.println("Podaj imię: ");
        tablica[numer].imie = scanner.next();
        System.out.println("Podaj nazwisko: ");
        tablica[numer].nazwisko = scanner.next();
        System.out.println("Podaj numer indeksu: ");
        tablica[numer].nr_indeksu = scanner.nextInt();
        System.out.println("Podaj specjalizację: ");
        tablica[numer].specjalizacja = scanner.next();
        System.out.println("Podaj rok studiów: ");
        tablica[numer].rok_studiow = scanner.nextInt();
        System.out.println("Dane studenta o indeksie " + numer + ": ");
        tablica[numer].wyswietlanie();
    }

    public void zmiana(){
        System.out.println("Podaj indeks (0-99)");
        numer = scanner.nextInt();
        System.out.println("Dane studenta o indeksie " + numer + ": ");
        tablica[numer].wyswietlanie();
        System.out.println("EDYCJA:\n");
        System.out.println("Podaj imię: ");
        tablica[numer].imie = scanner.next();
        System.out.println("Podaj nazwisko: ");
        tablica[numer].nazwisko = scanner.next();
        System.out.println("Numer indeksu: ");
        tablica[numer].nr_indeksu = scanner.nextInt();
        System.out.println("Podaj specjalizację: ");
        tablica[numer].specjalizacja = scanner.next();
        System.out.println("Podaj rok studiów: ");
        tablica[numer].rok_studiow = scanner.nextInt();
        System.out.println("Dane studenta o indeksie " + numer + " po zmianie: ");
        tablica[numer].wyswietlanie();
    }

    public void usuwanie(){
        System.out.println("Indeks studenta (0-99)");
        numer = scanner.nextInt();

        System.out.println("Czy chcesz usunąć dane studenta o indeksie " + numer + "?");
        System.out.println("t - tak, n - nie");
        wybor = scanner.next();
        if(wybor.equals("t")){
            tablica[numer].imie = "";
            tablica[numer].nazwisko = "";
            tablica[numer].nr_indeksu = 0;
            tablica[numer].specjalizacja  ="";
            tablica[numer].rok_studiow = 0;
            System.out.println("Dane usunięto");
        }
        else{
            System.out.println("Anulowano");
        }
    }

    public void wyswietl(){
        System.out.println("Podaj indeks(0-99)");
        numer = scanner.nextInt();
        System.out.println("Dane studenta o indeksie " + numer + ": ");
        tablica[numer].wyswietlanie();
    }

    public void wszyscy(){
        System.out.println("Dane studentów");
        for(int i = 0; i < tablica.length; i++){
            System.out.println("Dane studenta o indeksie " + i + ": ");
            tablica[i].wyswietlanie();
        }
    }

    public void zakres(){
        System.out.println("Zakres obiektów");
        poczatek = scanner.nextInt();
        koniec = scanner.nextInt();
        System.out.println("Dane studentów w zakresie od " + poczatek + " do " + koniec);
        for(int i = poczatek; i <= koniec; i++){
            System.out.println("Dane studenta o indeksie " + i + ": ");
            tablica[i].wyswietlanie();
        }
    }
}
