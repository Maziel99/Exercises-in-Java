/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wprowadzdane;

/**
 *
 * @author Ja
 */
public class Student {
    public String imie, nazwisko, specjalizacja;
    public int nr_indeksu, rok_studiow;
    public Student(String imie, String nazwisko, int nr_indeksu,
            String specjalizacja, int rok_studiow){
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.nr_indeksu = nr_indeksu;
        this.specjalizacja = specjalizacja;
        this.rok_studiow = rok_studiow;
        
    }
    
    public Student(String imie_, String nazwisko_, String specjalizacja_){
        imie = imie_;
        nazwisko = nazwisko_;
        specjalizacja = specjalizacja_;
    }
    
    public Student(int nr_indeksu1, String specjalizacja1, int rok_studiow1){
        nr_indeksu = nr_indeksu1;
        specjalizacja = specjalizacja1;
        rok_studiow = rok_studiow1;
        
    }
    
    public Student(String imie1, String nazwisko1, int rok_studiow2){
        imie = imie1;
        nazwisko = nazwisko1;
        rok_studiow = rok_studiow2;
    }
    
    public Student(String nazwisko2, String specjalizacja2){
        nazwisko = nazwisko2;
        specjalizacja = specjalizacja2;
    }
    
    public void wyswietlanie(){
        System.out.println("Imię: "+ imie);
        System.out.println("Nazwisko: "+ nazwisko);
        System.out.println("Numer indeksu: "+ nr_indeksu);
        System.out.println("Specjalizacja: "+ specjalizacja);
        System.out.println("Rok studiów: "+ rok_studiow);
    }
}
