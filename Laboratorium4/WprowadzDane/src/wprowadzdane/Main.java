/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wprowadzdane;

import java.util.Scanner;

/**
 *
 * @author Ja
 */
public class Main {
    public static  void main(String [] args){
        Tablica student = new Tablica();
        Scanner scanner = new Scanner(System.in);
        int x;

        student.tablica();

        while (true) {
            System.out.println("Wybierz co chesz zrobić:\n");
            System.out.println("1 - Wprowadź dane nowego studenta");
            System.out.println("2 - Edytuj dane studenta");
            System.out.println("3 - Usuń dane studenta");
            System.out.println("4 - Wyświetl dane studenta");
            System.out.println("5 - Wyświetl dane wszystkich studentów");
            System.out.println("6 - Wyświetl dane wszystkich studentów z określonego zakresu");

            x = scanner.nextInt();

            switch (x) {
                case 1: {
                    student.tworzenie();
                    break;
                }
                case 2: {
                    student.zmiana();
                    break;
                }
                case 3: {
                    student.usuwanie();
                    break;
                }
                case 4: {
                    student.wyswietl();
                    break;
                }
                case 5: {
                    student.wszyscy();
                    break;
                }
                case 6: {
                    student.zakres();
                    break;
                }
                default: {
                    System.out.println("Niewłaściwa operacja");
                    break;
                }
            }
        }
    }
}
