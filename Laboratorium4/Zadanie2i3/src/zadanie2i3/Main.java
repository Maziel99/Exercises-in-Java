/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zadanie2i3;

/**
 *
 * @author Ja
 */
public class Main {
    public static void main(String[] args) {
        Student student_1 = new Student("Grzegorz", "Brzęczyszczykiewicz",
                "retoryka");
        
        Student student_2 = new Student(101101, "Cyberbezpieczeństwo", 3);
        
        Student student_3 = new Student("Harrier", "DuBois", 5);
        
        Student student_4 = new Student("Nowak", "Sieci komputerowe");
        
        
        
        student_1.wyswietlanie();
        student_2.wyswietlanie();
        student_3.wyswietlanie();
        student_4.wyswietlanie();
        
        
    }
}
