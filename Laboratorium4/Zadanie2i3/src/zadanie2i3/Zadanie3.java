/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zadanie2i3;

import java.util.Scanner;

/**
 *
 * @author Ja
 */
public class Zadanie3 {
    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Podaj imię: ");
        String imie = scanner.next();
        
        
        System.out.println("Podaj nazwisko: ");
        String nazwisko = scanner.next();
        
        
        System.out.println("Podaj numer indeksu: ");
        int nr_indeksu = scanner.nextInt();
        
        
        System.out.println("Podaj specjalizację: ");
        String specjalizacja = scanner.next();
        
        
        System.out.println("Podaj rok studiów: ");
        int rok_studiow = scanner.nextInt();
        
        
        Student student = new Student(imie, nazwisko, nr_indeksu, specjalizacja,
                rok_studiow);
        student.wyswietlanie();
                
    }
}
