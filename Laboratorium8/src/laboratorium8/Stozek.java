/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium8;

/**
 *
 * @author Ja
 */
public class Stozek extends FiguraPrzestrzenna{
    private double r, l, H;
    
    public Stozek(double r, double l, double H){
        this.r = r;
        this.l = l;
        this.H = H;
    }
    
    @Override
    public double obliczObjetosc(){
        return (1/3) * r * r * H * Math.PI;
    }
    
    @Override
    public double obliczPole(){
        return r * (r + l) * Math.PI;
    }
    
    
    @Override
    public String toString(){
        String s = super.toString();
        s += "\nStozek o wymiarach " + r + " " + l + " " + H;
        s += " ma pole " + this.obliczPole();
        s += "i objetosc " + this.obliczObjetosc();
        return s;
    }
}
