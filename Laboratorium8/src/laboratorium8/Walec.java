/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium8;

/**
 *
 * @author Ja
 */
public class Walec extends FiguraPrzestrzenna{
    private double r, H;
    
    public Walec(double a, double b, double H){
        this.r = r;
        this.H = H;
    }
    
    @Override
    public double obliczObjetosc(){
        return r * r * H * Math.PI;
    }
    
    @Override
    public double obliczPole(){
        return 2 * r * (r + H) * Math.PI;
    }
    
    
    @Override
    public String toString(){
        String s = super.toString();
        s += "\nWalec o wymiarach " + r + " " + H;
        s += " ma pole " + this.obliczPole();
        s += "i objetosc " + this.obliczObjetosc();
        return s;
    }
}
