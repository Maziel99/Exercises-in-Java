/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium8;

/**
 *
 * @author pracownik
 */
public class Laboratorium8 {
    public void abc() {
        System.out.println("abc");
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Laboratorium8 l = new Laboratorium8();
        l.abc();
        // błąd - nie można tworzyć instancji klasy abstrakcyjnej
        //Product p = new Product("Laptop", "Jest to ...", 10);
        
        // tworzymy instancję klasy Tractor i wywołujemy jej metody
        Tractor t = new Tractor("URSUS", "opis", 50000);
        t.buy();
        t.showInfo();
        
        // instancję klasy Tractor możemy przypisać do typu klasy
        // abstrakcyjnej. Możemy wywoływać metody abstrakcyjne, które
        // mają już konkretną implementację
        Product p = new Tractor("URSUS", "opis", 50000);
        p.buy();
        p.showInfo();
        
        Kwadrat k = new Kwadrat(10);
        System.out.println("Obwód kwadratu o boku 10 = " + k.obliczObwod());
        System.out.println("Pole kwadratu o boku 10 = " + k.obliczPole());
        System.out.println(k);
        
        Trojkat tr = new Trojkat(3, 4, 5, 4);
        System.out.println("Obwód trójkąta o bokach 3, 4, 5 = " + tr.obliczObwod());
        System.out.println("Pole trójkąta o bokach 3, 4, 5 =" + tr.obliczPole());
        
        Szescian sz = new Szescian(10.0);
        System.out.println(sz);
        
        FiguraPlaska [] figuryPlaskie = new FiguraPlaska[2];
        figuryPlaskie[0] = k;
        figuryPlaskie[1] = tr;
        
        for (int i = 0; i < figuryPlaskie.length; i++) {
            System.out.println("Figura to " + figuryPlaskie[i].nazwaFigury());
            System.out.println("Jej pole wynosi = " + figuryPlaskie[i].obliczPole());
            System.out.println("Jej obwód wynosi = " + figuryPlaskie[i].obliczObwod());
        }
    }
    
}
