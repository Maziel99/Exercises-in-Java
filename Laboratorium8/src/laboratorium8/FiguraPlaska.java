/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium8;

/**
 *
 * @author pracownik
 */
public abstract class FiguraPlaska extends FiguraGeometryczna {
    public abstract double obliczObwod(); 
    public abstract String nazwaFigury();
    
    @Override
    public String toString() {
        return "Klasa obliczającą parametry figury płaskiej";
    }
}
