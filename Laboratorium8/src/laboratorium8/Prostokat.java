/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium8;

/**
 *
 * @author Ja
 */
public class Prostokat extends FiguraPlaska{
    private double a, b;
    
    public Prostokat(double a, double b){
        this.a = a;
        this.b = b;
    }
    
    @Override
    public double obliczObwod(){
        return 2*a+2*b;
    }
    
    @Override
    public double obliczPole(){
        return a * b;
    }
    
     @Override
    public String nazwaFigury(){
        return "Prostokat";
    }
    
    @Override
    public String toString(){
        String s = super.toString();
        s += "\nProstokat o bokach " + a + " " + b;
        s += " ma pole " + this.obliczPole();
        s += "i obwód " + this.obliczObwod();
        return s;
    }
}
