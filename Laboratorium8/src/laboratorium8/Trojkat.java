/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium8;

/**
 *
 * @author pracownik
 */
public class Trojkat extends FiguraPlaska {
    private double a, b, c, h;
    
    public Trojkat(double a, double b, double c, double h) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.h = h;
    }

    @Override
    public double obliczObwod() {
        return a + b + c;
    }

    @Override
    public double obliczPole() {
        return 0.5 * a * h;
    }
    
    @Override
    public String nazwaFigury() {
        return "Trójkąt";
    }
    
}
