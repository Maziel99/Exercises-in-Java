/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium8;

/**
 *
 * @author pracownik
 */
public class Szescian extends FiguraPrzestrzenna {
    private double a;
    
    public Szescian(double a) {
        this.a = a;
    }
    
    @Override
    public double obliczObjetosc() {
        return a * a * a;
    }

    @Override
    public double obliczPole() {
        return 6 * a * a;
    }
    
    @Override
    public String toString() {
        String s = super.toString();
        s += "\nSześcian o boku " + a;
        s += " ma pole = " + this.obliczPole();
        s += " i objętość = " + this.obliczObjetosc();
        return s;
    }
}
