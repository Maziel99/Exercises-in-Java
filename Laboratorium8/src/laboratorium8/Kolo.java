/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium8;

/**
 *
 * @author Ja
 */
public class Kolo extends FiguraPlaska{
    private double r;
    
    public Kolo(double r){
        this.r = r;
    }
    
    @Override
    public double obliczObwod(){
        return 2 * r * Math.PI;
    }
    
    @Override
    public double obliczPole(){
        return r * r * Math.PI;
    }
    
     @Override
    public String nazwaFigury(){
        return "Kolo";
    }
    
    @Override
    public String toString(){
        String s = super.toString();
        s += "\nKolo o promieniu " + r;
        s += " ma pole " + this.obliczPole();
        s += "i obwód " + this.obliczObwod();
        return s;
    }
}
