/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium8;

/**
 *
 * @author Ja
 */
public class Prostopadloscian extends FiguraPrzestrzenna{
    private double a, b, H;
    
    public Prostopadloscian(double a, double b, double H){
        this.a = a;
        this.b = b;
        this.H = H;
    }
    
    @Override
    public double obliczObjetosc(){
        return a * b * H;
    }
    
    @Override
    public double obliczPole(){
        return 2 * a * b + 2 * b * H + 2 * a * H;
    }
    
    
    @Override
    public String toString(){
        String s = super.toString();
        s += "\nProstopadloscian o wymiarach " + a + " " + b + " " + H;
        s += " ma pole " + this.obliczPole();
        s += "i objetosc " + this.obliczObjetosc();
        return s;
    }
}
