/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium8;

/**
 *
 * @author Ja
 */
public class Romb extends FiguraPlaska{
    private double a, h;
    
    public Romb(double a, double h){
        this.a = a;
        this.h = h;
    }
    
    @Override
    public double obliczObwod(){
        return 4 * a;
    }
    
    @Override
    public double obliczPole(){
        return a * h;
    }
    
     @Override
    public String nazwaFigury(){
        return "Romb";
    }
    
    @Override
    public String toString(){
        String s = super.toString();
        s += "\nRomb o bokach " + a + " " + h;
        s += " ma pole " + this.obliczPole();
        s += "i obwód " + this.obliczObwod();
        return s;
    }
}
