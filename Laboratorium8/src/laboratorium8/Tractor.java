/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium8;

/**
 *
 * @author pracownik
 */
public class Tractor extends Product {
    public Tractor(String name, String describe, double price){
        super(name, describe, price);
    }
    
    @Override
    public void buy(){
        System.out.println("I bought a black tractor, capaciticy 2400!");
    }
    
    @Override
    public void showInfo(){
        System.out.println("Price:"+getPrice()+" name:"+getName()+" describe:"+getDescribe());
    }
}