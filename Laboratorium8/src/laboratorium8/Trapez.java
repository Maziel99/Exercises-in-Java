/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium8;

/**
 *
 * @author Ja
 */
public class Trapez extends FiguraPlaska{
    private double a, b, c, d, h;
    
    public Trapez(double a, double b,double c, double d, double h){
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.h = h;
    }
    
    @Override
    public double obliczObwod(){
        return a + b + c + d;
    }
    
    @Override
    public double obliczPole(){
        return (a+b)*0.5*h;
    }
    
     @Override
    public String nazwaFigury(){
        return "Trapez";
    }
    
    @Override
    public String toString(){
        String s = super.toString();
        s += "\nTrapez o bokach " + a + " " +  b  + " " + c + " " + d + " "+ h;
        s += " ma pole " + this.obliczPole();
        s += "i obwód " + this.obliczObwod();
        return s;
    }
}
