/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium8;

/**
 *
 * @author Ja
 */
public class Rownoleglobok extends FiguraPlaska{
    private double a, b, h;
    
    public Rownoleglobok(double a, double b, double h){
        this.a = a;
        this.b = b;
        this.h = h;
    }
    
    @Override
    public double obliczObwod(){
        return 2 * a + 2 * b;
    }
    
    @Override
    public double obliczPole(){
        return a * h;
    }
    
     @Override
    public String nazwaFigury(){
        return "Rownoleglobok";
    }
    
    @Override
    public String toString(){
        String s = super.toString();
        s += "\nRownoleglobok o bokach " + a + " " + b + " " + h;
        s += " ma pole " + this.obliczPole();
        s += "i obwód " + this.obliczObwod();
        return s;
    }
}
