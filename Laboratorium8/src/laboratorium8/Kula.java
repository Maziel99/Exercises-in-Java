/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium8;

/**
 *
 * @author Ja
 */
public class Kula extends FiguraPrzestrzenna{
    private double R;
    
    public Kula(double R){
        this.R = R;
    }
    
    @Override
    public double obliczObjetosc(){
        return (4/3) * R * R * R * Math.PI;
    }
    
    @Override
    public double obliczPole(){
        return 4 * R * R * Math.PI;
    }
    
    @Override
    public String toString(){
        String s = super.toString();
        s += "\nKula o promieniu " + R;
        s += " ma pole " + this.obliczPole();
        s += "i objetosc " + this.obliczObjetosc();
        return s;
    }
}
