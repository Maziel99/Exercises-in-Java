/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium7;

/**
 *
 * @author Ja
 */
public class Osoba {
    public String imie, nazwisko, dataNarodzin, plec;
       
    public Osoba(String imie, String nazwisko, String dataNarodzin, String plec){
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.dataNarodzin = dataNarodzin;
        this.plec = plec;
    }
    
    @Override
    public String toString(){
        StringBuilder data = new StringBuilder();
        data.append(this.imie);
        data.append(" ");
        data.append(this.nazwisko);
        data.append(", ");
        data.append(this.dataNarodzin);
        data.append(", ");
        data.append(this.plec);
        return data.toString();
    }
    
}
