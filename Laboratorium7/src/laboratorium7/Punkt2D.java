/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium7;

import java.util.Random;

/**
 *
 * @author pracownik
 */
public class Punkt2D {
    private double x, y;
    
    public Punkt2D() {
        x = 0;
        y = 0;
    }
    
    public Punkt2D(double x, double y) {
        this.x = x;
        this.y = y;
    }
    
    public void losujWspolrzedne() {
        Random random = new Random();
        Random random2 = new Random();
        double losowaWartosc = -10, losowaWartosc2 = -10;
        do {
            losowaWartosc = random.nextInt();
        } while(losowaWartosc < -10 || losowaWartosc > 10);
        
        do {
            losowaWartosc2 = random2.nextInt();
        } while(losowaWartosc2 < -10 || losowaWartosc2 > 10);
        
        this.x = losowaWartosc;
        this.y = losowaWartosc2;
    }
    
    @Override
    public String toString() {
        String s = "Punkt ma współrzędne ";
        s += "(" + x + ", " + y + ")";
        return s;
    }
}
