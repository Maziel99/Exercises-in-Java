/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium7;

import java.util.Random;

/**
 *
 * @author pracownik
 */
public class Laboratorium7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // ustawiono ziarno generatora liczb pseudolosowych
        // skutkuje to tym, że generator generuje zawsze te same
        // liczby
        Random r = new Random(10);
        System.out.println(r.nextInt());
        System.out.println(r.nextInt());
        System.out.println(r.nextInt());
        
        // nie ustawiono ziarna generatora liczb pseudolosowych
        // skutkuje to tym, że generator generuje różne liczby
        // przy każdym uruchomieniu
        Random r2 = new Random();
        System.out.println(r2.nextInt());
        System.out.println(r2.nextInt());
        System.out.println(r2.nextInt());
        
        Punkt2D p = new Punkt2D();
        p.losujWspolrzedne();
        System.out.println(p);
        
        Punkt3D p2 = new Punkt3D();
        p2.losujWspolrzedne();
        System.out.println(p2);
        
        Punkt2D [] array2D = new Punkt2D[100];
        Punkt3D [] array3D = new Punkt3D[100];
        
        for(int i = 0; i < array2D.length; i++)
        {
            array2D[i] = new Punkt2D();
            array2D[i].losujWspolrzedne();
        }
        
        for(int i = 0; i < array3D.length; i++)
        {
            array3D[i] = new Punkt3D();
            array3D[i].losujWspolrzedne();
        }
        
        
        for(int i = 0; i < array2D.length; i++){
            for(int j = 0; j <array3D.length; j++){
                if(array2D[i].hashCode() == array3D[j].hashCode() && array2D[i].equals(array3D[j])){
                    System.out.println(array3D[j] + " i " + array2D[i] + " są o tych samych współrzędnych (x, y)");
                }
            }        
       
    }
  }  
}
