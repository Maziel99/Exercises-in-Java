/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium7;

import java.util.Random;

/**
 *
 * @author pracownik
 */
public class Punkt3D extends Punkt2D {
    private double z;
    
    public Punkt3D() {
        super();
        z = 0;
    }
    
    public Punkt3D(double x, double y, double z) {
        super(x, y);
        this.z = z;
    }
    
    @Override
    public void losujWspolrzedne() {
        super.losujWspolrzedne();
        Random random = new Random();
        double losowaWartosc;
        do {
            losowaWartosc = random.nextInt();
        } while(losowaWartosc < -10 || losowaWartosc > 10);
        
        this.z = losowaWartosc;
    }
    
    @Override
    public String toString() {
        String s = super.toString();
        s = s.replace(")", ", ");
        s += z + ")";
        return s;
    }
}
