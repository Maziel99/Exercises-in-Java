/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium7;

/**
 *
 * @author Ja
 */
public class Student extends Osoba{
    public String indeks, typ, kierunek, rok;
    
    public Student(String imie, String nazwisko, String dataNarodzin, String plec,String indeks, String typ, String kierunek, String rok){
        super(imie, nazwisko, dataNarodzin, plec);
        this.indeks = indeks;
        this.typ = typ;
        this.kierunek = kierunek;
        this.rok = rok;
        
    }
    
    
    @Override
    public String toString(){
        StringBuilder studentData = new StringBuilder();
        studentData.append(", ");
        studentData.append(this.indeks);
        studentData.append(", ");
        studentData.append(this.typ);
        studentData.append(", ");
        studentData.append(this.kierunek);
        studentData.append(", ");
        studentData.append(this.rok);
        return super.toString() + studentData.toString();
    }
}
