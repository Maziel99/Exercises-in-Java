/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium7;

/**
 *
 * @author Ja
 */
public class Wykladowca extends Osoba{
    public String stopien, zatrudnienie, zajecia;
    
    public Wykladowca(String imie, String nazwisko, String dataNarodzin, String plec,
                      String stopien, String zatrudnienie, String zajecia){
    
    super (imie, nazwisko, dataNarodzin, plec);
    this.stopien = stopien;
    this.zatrudnienie = zatrudnienie;
    this.zajecia = zajecia;
    }
    
    
@Override
public String toString()
{
    StringBuilder wykladowcaData = new StringBuilder();
    wykladowcaData.append(", ");
    wykladowcaData.append(stopien);
    wykladowcaData.append(", ");
    wykladowcaData.append(zatrudnienie);
    wykladowcaData.append(", ");
    wykladowcaData.append(zajecia);
    return super.toString() + wykladowcaData.toString();
}
}
