/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium6;

/**
 *
 * @author pracownik
 */
public class Complex {
    private double a, b;
    
    public Complex(double a, double b) {
        this.a = a;
        this.b = b;
    }
    
    public static double modul(Complex complex) {
        return Math.sqrt(Math.pow(complex.a, 2) + Math.pow(complex.b, 2));
    }
    
    public static Complex sprzezenie(Complex complex) {
        return new Complex(complex.a, -complex.b);
    }
    
    public static Complex addComplex(Complex complex, Complex complex2) {
        return new Complex(complex.a + complex2.a, complex.b + complex2.b);
    }
    
    public static Complex multiplyComplex(Complex complex, Complex complex2) {
        double a = (complex.a * complex2.a) - (complex.b * complex2.b);
        double b = (complex.b * complex2.a) + (complex.a * complex2.b);
        
        return new Complex(a, b);
    }
    
    public static Complex subtractionComplex(Complex complex, Complex complex2) {
        return new Complex(complex.a - complex2.a, complex.b - complex2.b);
    }
    
    public static Complex divideComplex(Complex complex, Complex complex2){
        Complex nominator = multiplyComplex(complex, sprzezenie(complex2));
        Complex denominator = multiplyComplex(complex2, sprzezenie(complex2));
        double a = nominator.a/(denominator.a + denominator.b);
        double b = nominator.b/(denominator.a + denominator.b);
        
        return new Complex(a, b);
    }
}
