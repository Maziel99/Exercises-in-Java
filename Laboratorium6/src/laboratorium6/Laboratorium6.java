/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium6;

/**
 *
 * @author pracownik
 */
public class Laboratorium6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("obwód koła o promieniu 5 = " + Figury.obwodKola(5));
        System.out.println("pole koła o promieniu 5 = " + Figury.poleKola(5));
        System.out.println("obwód kwadratu o boku 5 = " + Figury.obwodKwadratu(5));
        System.out.println("pole kwadratu o boku 5 = " + Figury.poleKwadratu(5));
        System.out.println("pole prostokąta = " + Figury.poleProstokata(7, 10));
        System.out.println("obowd prostokąta = " + Figury.obwodProstokata(10, 7));
        System.out.println("pole stożka = " + Figury.poleStozka(5,6,8));
        System.out.println("objętość stożka = " + Figury.objetoscStozka(5,9));
        System.out.println("objętość walca = " + Figury.objetoscWalca(5,10));
        System.out.println("pole walca = " + Figury.poleWalca(10,5));
        
        Punkt p = new Punkt(10, 20, 30);
        Punkt p2 = new Punkt(11, 22, 33);
        
        Punkt.pokazOstatniObiekt();
        System.out.println(p.toString());
        System.out.println(p2.toString());
        
        // odczytanie publicznego pola statycznego
        // zwracana jest instancja klasy, więc mamy dostęp
        // do metod niestatycznych
        System.out.println(Punkt.lastAddedPoint.toString());
        // próba odczytania prywatnego pola statycznego - błąd
        // System.out.println(Punkt.counter);
        
        System.out.println("Moduł liczby zespolonej = " + 
                Complex.modul(new Complex(1, 2)));
        System.out.println("(1,2) * (2, 5) = " + Complex.multiplyComplex(new Complex(1, 2), new Complex(2, 5))); 
        Complex c = new Complex(2, 6);
        System.out.println("(1,2) - (2, 5) = " + Complex.subtractionComplex(new Complex(1, 2), new Complex(2, 5)));
        System.out.println("(1,2) : (2, 5) = " + Complex.divideComplex(new Complex(1, 2), new Complex(2, 5)));
    }
    
}
