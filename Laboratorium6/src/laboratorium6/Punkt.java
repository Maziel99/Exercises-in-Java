/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium6;

/**
 *
 * @author pracownik
 */
public class Punkt {
    private double x, y, z;
    private static int counter;
    public static Punkt lastAddedPoint;
    
    public Punkt(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
        
        counter++;
        lastAddedPoint = this;
    }
    
    public static void pokazOstatniObiekt() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Ostatnio dodano punkt o współrzędnych x = ");
        stringBuilder.append(lastAddedPoint.x);
        stringBuilder.append(", y = ");
        stringBuilder.append(lastAddedPoint.y);
        stringBuilder.append(", z = ");
        stringBuilder.append(lastAddedPoint.z);
        stringBuilder.append(".\n Łącznie istnieje ");
        stringBuilder.append(counter);
        stringBuilder.append(" instancji tej klasy");
        System.out.println(stringBuilder.toString());
    }
    
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Punkt o współrzędnych x = ");
        stringBuilder.append(this.x);
        stringBuilder.append(", y = ");
        stringBuilder.append(this.y);
        stringBuilder.append(", z = ");
        stringBuilder.append(this.z);
        stringBuilder.append(".\n Łącznie istnieje ");
        stringBuilder.append(counter);
        stringBuilder.append(" instancji tej klasy");
        return stringBuilder.toString();
    }
    
}
