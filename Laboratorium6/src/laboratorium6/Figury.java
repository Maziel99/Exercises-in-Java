/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium6;

/**
 *
 * @author pracownik
 */
public class Figury {
    static {
        System.out.println("Biblioteka obliczająca wielkości figur geometrycznych");
    }
    
    public static double poleKola(double r) {
        return Math.PI * Math.pow(r, 2);
    }
    
    public static double obwodKola(double r) {
        return 2 * Math.PI * r;
    }
    
    public static double obwodKwadratu(double a)
    {
        return 4*a;
    }
    
     public static double poleKwadratu(double a)
    {
        return Math.pow(a, 2);
    }
    
    public static double poleProstokata(double a, double b)
    {
        return a*b;
    }
    
    public static double obwodProstokata(double a, double b)
    {
        return (2*a)+(2*b);
    }
    
    public static double poleStozka(double l, double r, double h)
    {
        return Math.PI*r*(r+l);
    }
    
    public static double objetoscStozka(double r, double h)
    {
        return ((Math.pow(r, 2))*(1.0/3)*Math.PI*h);
    }
    
    public static double objetoscWalca(double r, double h)
    {
        return (Math.PI*Math.pow(r, 2)*h);
    }
    
    public static double poleWalca(double r, double h)
    {
        return (2*Math.PI*Math.pow(r,2)+2*Math.PI*r*h);
    }
    

}
