/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium3;

/**
 *
 * @author Ja
 */
public class Zadanie11 {
    
    
        public static void main(String[] arg){
        double a=12.12;
        System.out.println(a--);
        System.out.println(a++);
        System.out.println(--a);
        System.out.println(++a);
        
        System.out.println("Przy a-- i a++ najpierw dochodzi do wykonania danej"
                +" instrukcji, a dopiero po jej wykonaniu zachodzi zmiana wartości");
        
        System.out.println("Przy --a i ++a najpierw dochodzi do zmiany wartości"
                +", a po niej dopiero wykonanie danej instrukcji");
        
}
}
