/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium3;

import java.util.Scanner;

/**
 *
 * @author Ja
 */
public class Zadanie32 {
    public static void main(String [] args){
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int silnia = 1;

        if(n == 0){
            silnia = 1;
        }

        for(int i = 1; i <= n; i++){
                silnia *= i;
        }
        System.out.println(n + "! = " + silnia);
    }
}
