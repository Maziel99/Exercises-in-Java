/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium3;

/**
 *
 * @author pracownik
 */
public class Zadanie34 {
    public static int rekurencjaNieskończona(int n) {
        return rekurencjaNieskończona(n + 1);
    }
    
    public static int fib(int n) {
        if (n == 0) return 0;
        if (n == 1) return 1;
        return fib(n-1) + fib(n-2);
    }
    
    public static void main(String[] args){
        int n = 0;
        System.out.println("fib(0)=" + fib(0));
        n = 1;
        System.out.println("fib(1)=" + fib(1));
        n = 2;
        System.out.println("fib(2)=" + fib(2));
        n = 3;
        System.out.println("fib(3)=" + fib(3));
        
        System.out.println(fib(1000));
        
        System.out.println(rekurencjaNieskończona(1));
    }
}
