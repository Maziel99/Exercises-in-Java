/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium3;

/**
 *
 * @author pracownik
 */
public class Zadanie22String {
    // możliwość używania polskich znaków w kodzie Java
    // nie jest to jednak polecane
    public static boolean metodą() {
        return false;
    }
    
    public static void main(String [] args) {
    String example = "Example String";
    // zamienia obiekt klasy String na tablicę typu char
    char [] arrayOfChar = example.toCharArray();
    System.out.println(arrayOfChar[0]);
    
    // szybszy sposób na pobranie n-tego znaku
    System.out.println(example.charAt(0));
    
    // zamienia duże litery w ciągu znaków na małe
    System.out.println(example.toLowerCase());
    
    // porównywanie obiektów klasy String
    // zwróci true, tylko jeżeli wszystkie znaki w ciągach
    // są takie same
    System.out.println("Example Strin".equals(example));
    System.out.println("Example String".equals(example));
    System.out.println(!"Example Strin".equals(example));
    
    // wyszukuje wystąpienie podciągu znaków
    // w przypadku znalezienia zwraca indeks znaku,
    // gdzie rozpoczyna się dany podciąg
    // w przypadku nieznalezienia podciągu, zwraca -1
    System.out.println(example.indexOf("String"));
    System.out.println(example.indexOf("not present"));
    
    // wyszukuje wystąpienie konkretnego znaku
    // podajemy kod unicode
    System.out.println(example.indexOf(97));
    System.out.println(97 == 'a');
    System.out.println('ą' == 97);
    
    }
}
