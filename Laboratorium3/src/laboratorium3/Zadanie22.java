/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium3;

/**
 *
 * @author pracownik
 */

public class Zadanie22 {
    public static boolean closeDoor;
    
    public static boolean pushedFloor(boolean isPushed) {
        closeDoor = true;
        System.out.println("Zamykam drzwi");
        return isPushed;
    }
    public static boolean pushedClosedDoor(boolean isPushed) {
        closeDoor = true;
        System.out.println("Zamykam drzwi");
        return isPushed;
    }
    public static boolean thereIsObjectOnEntrance(boolean isObject) {
        closeDoor = !isObject;
        System.out.println("Nie zamykam drzwi!");
        return isObject;
    }
    
    public static boolean sample() {
        System.out.println("I tak się wykonam dla &");
        return false;
    }
    
    public static void main(String [] args) { 
        System.out.println(false & true);
        System.out.println(false && true);
        System.out.println(false & sample());
        System.out.println(false && sample());
        
        System.out.println(pushedFloor(true) & pushedClosedDoor(false) & 
                thereIsObjectOnEntrance(true));
        System.out.println(pushedFloor(true) && pushedClosedDoor(false) && 
                thereIsObjectOnEntrance(true));
        
        byte a = 0, b = 10;
        for (byte i = -128; i < 128; i++) {
            System.out.println(a + " & " + i + " = " + (a & i) + " " +
                    Integer.toBinaryString((int)(a & i)));
            if (i == 127) break;
        }
        System.out.println("");
        System.out.println("");
        for (byte i = -128; i < 128; i++) {
            System.out.println(b + " & " + i + " = " + (b & i) +
                    " " + Integer.toBinaryString((int)(b & i)));
            if (i == 127) break;
        }
        
       System.out.println(false | true);
        System.out.println(false || true);
        System.out.println(false | sample());
        System.out.println(false || sample());
        
        System.out.println(pushedFloor(true) | pushedClosedDoor(false) | 
                thereIsObjectOnEntrance(true));
        System.out.println(pushedFloor(true) || pushedClosedDoor(false) || 
                thereIsObjectOnEntrance(true));
        
        
        for (byte i = -128; i < 128; i++) {
            System.out.println(a + " | " + i + " = " + (a | i) + " " +
                    Integer.toBinaryString((int)(a & i)));
            if (i == 127) break;
        }
        System.out.println("");
        System.out.println("");
        for (byte i = -128; i < 128; i++) {
            System.out.println(b + " | " + i + " = " + (b | i) +
                    " " + Integer.toBinaryString((int)(b | i)));
            if (i == 127) break;
        }
        
        
    }
    
    
    
}
