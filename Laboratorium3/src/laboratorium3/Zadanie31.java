/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium3;

import java.math.BigInteger;
import java.util.Scanner;

/**
 *
 * @author pracownik
 */
public class Zadanie31 {
    public static long silniaIteracyjnie(long wartosc) {
        if (wartosc == 0) {
            return 1;
        }
        long silnia = 1;
        for (long i = 1; i <= wartosc; i++) {
            silnia *= i;
        }
        return silnia;
    }
    
    public static BigInteger silniaIteracyjnieBigInt(BigInteger big) {
        if (big.equals(BigInteger.ZERO)) {
            return BigInteger.valueOf(1);
        }
        BigInteger silnia = BigInteger.valueOf(1);
        for (long i = 1; i <= big.longValue(); i++) {
            silnia = silnia.multiply(BigInteger.valueOf(i));
        }
        return silnia;
    }
    
    public static int silnia(int wartosc) {
    // jeśli przekazany parametr jest równy zero to zwróć 1
    // a w przeciwnym wypadku zwróć wartość parametru * wywołanie metody silnia
    // z parametrem o jeden mniejszym
    if (wartosc == 0) return 1;
    return wartosc * silnia(wartosc - 1);
    }
    
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.print("Podaj liczbę: " );
        // pobieramy od użytkownika liczbę
        int liczba = sc.nextInt();
        // wyświetlamy na ekranie obliczoną silnię
        System.out.println(liczba + "! = " + silnia(liczba));
        // wersja na wartościach long, jest w stanie zaprezentować
        // większe liczby
        System.out.println(silniaIteracyjnie((long)liczba));
        
        // wersja z użyciem klasy BigInteger, jest w stanie reprezentować
        // liczby większe niż long
        System.out.println(silniaIteracyjnieBigInt(new BigInteger(String.valueOf(liczba)))
                .toString());
    }
}
