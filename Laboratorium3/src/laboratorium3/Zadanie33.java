/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorium3;

import java.util.Scanner;

/**
 *
 * @author Ja
 */
public class Zadanie33 {
    public static int potega(int n, int x){
        int wynik;
        if(x == 0) return 1;
        if(x % 2 == 1) return n * potega(n, x-1);
        else {
            wynik = potega(n, x/2);
            return wynik*wynik;
        }
    }
     public static void main(String [] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podstawa potęgi: ");
        int podstawa = scanner.nextInt();
        System.out.println("Wykładnik potęgi: ");
        int wykladnik = scanner.nextInt();
        System.out.println("Rekurencyjnie:");
        System.out.println(podstawa+" ^ "+wykladnik+" = "+potega(podstawa, wykladnik));

        
        System.out.println("Iteracyjnie:");
        int a = podstawa;
        if(wykladnik != 0) {
            for (int i = 1; i < wykladnik; i++) {
                podstawa *= a;
            }
        }
        else podstawa = 1;
        System.out.println(podstawa);
    }
} 

